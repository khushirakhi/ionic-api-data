import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../api-service.service';

@Component({
  selector: 'app-student-details',
  templateUrl: './student-details.page.html',
  styleUrls: ['./student-details.page.scss'],
})
export class StudentDetailsPage implements OnInit {

  constructor(private apiService : ApiServiceService) { }
  name :any;
  
  ngOnInit() {
    this.name = this.apiService.getStudent();
  }

}
