import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const apiData : any = "http://jsonplaceholder.typicode.com/todos";
@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {

  private student;
  
  constructor(private http: HttpClient) { }
  
  getAll() {
    return this.http.get(apiData);
  }

  setStudent(data)
  {
    this.student = data;
  }

  getStudent()
  {
    return this.student;
  }
}
